package spellingbee.client;

import spellingbee.network.*;
import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;

/**
 * GameTab represents the game tab of the spelling bee game, where the user will be able to play the game.
 * It stores the JavaFX components required for the game to function properly.
 * @author David Nguyen
 *
 */
public class GameTab extends Tab{
	private Client gameClient;
	private TextField userWordField;
	private TextField message;
	private TextField currentScore;
	private String userWord;
	private Button submitButton;
	
	/**
	 * GameTab constructor that initiates the JavaFX components and adds them to the game tab.
	 * @param gameClient the client which communicates with the server
	 */
	public GameTab(Client gameClient) {
		super("Game");
		this.gameClient = gameClient;
		this.userWordField = new TextField();
		this.message = new TextField("-");
		this.currentScore = new TextField("0");
		this.userWord = "";
		this.submitButton = new Button("Submit");
		this.setContent(addContent());
	}
	/**
	 * addContent adds all the JavaFX components into a VBox and returns the VBox.
	 * The method also calls certain methods to add event listeners.
	 * @return VBox a vbox containing all of JavaFX components with its event listeners.
	 */
	private VBox addContent() {
		VBox allContent = new VBox();
		//calls addSubmitEvent to add an event listener to submitButton
		submitButton = addSubmitEvent(submitButton);
		allContent.getChildren().addAll(addLetterButtons(), userWordField, submitButton,addMessageAndScore());
		return allContent;
	}
	
	/**
	 * addLetterButtons retrieves all letters from the server and makes buttons for each of those letters.
	 * @return HBox an hbox containing all the buttons for each letter given
	 */
	private HBox addLetterButtons() {
		String response = gameClient.sendAndWaitMessage("getAllLetters");
		//calls parseLetters in order to seperate the letters from the string
		String[] responseParse = parseLetters(response);
		HBox allLetters = new HBox();
		Button[] allButtons = new Button[responseParse.length];
		
		//loops through allButtons and assigns each letter to a button
		for(int i = 0; i < allButtons.length; i++) {
			allButtons[i] = new Button(responseParse[i]);
			//if the letter is the center letter, then it will be colored
			if(verifyCenter(responseParse[i])) {
				allButtons[i].setStyle("-fx-text-fill: #f7005f");
			}
			//calls addLetterEvent to add an event listener to the current button
			allButtons[i] = addLetterEvent(allButtons[i], responseParse[i]);
			allLetters.getChildren().add(allButtons[i]);
		}
		
		return allLetters;
		
		
	}
	
	/**
	 * addLetterEvent adds an event listener to the button being sent.
	 * When an event is triggered, the event listener adds the letter that has been pressed to the user attempt text field.
	 * @param currentButton the button where the event listener will be added to
	 * @param letter the letter to add 
	 * @return Button the button with the event listener
	 */
	private Button addLetterEvent(Button currentButton, String letter) {
		currentButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				userWord = userWordField.getText();
				userWord+=letter;
				userWordField.setText(userWord);
			}
		});
		return currentButton;
	}
	
	/**w
	 * addSubmitEvent adds an event listener to the button being sent.
	 * When an event is triggered, the event listener retrieves how many points the word attempt is worth and if it's valid from the server.
	 * @param currentButton the current button
	 * @return Button the button with the event listener
	 */
	private Button addSubmitEvent(Button currentButton) {
		currentButton.setOnAction(new EventHandler<ActionEvent>(){
			@Override
			public void handle(ActionEvent e) {
				//calls gameClient to send to the server if the word is valid and how many point it's worth
				message.setText(gameClient.sendAndWaitMessage("getMessage;" + userWordField.getText()));
				gameClient.sendAndWaitMessage("getPointsForWord;" + userWordField.getText());
				currentScore.setText(gameClient.sendAndWaitMessage("getScore"));
			}
		});
		return currentButton;
	}
	
	/**
	 * addMessageAndScore creates an HBox and adds the message and current score text field to it.
	 * @return HBox an hbox with the message and score
	 */
	private HBox addMessageAndScore() {
		HBox messageAndScore = new HBox();
		messageAndScore.getChildren().addAll(message,currentScore);
		return messageAndScore;
	}
	
	/**
	 * getScoreField returns the text field in which the score is displayed
	 * This is used for the score tab to update the user's current score.
	 * @return TextField the score field
	 */
	public TextField getScoreField() {
		return this.currentScore;
	}
	/*HELPER METHODS*/
	/**
	 * parseLetters takes the server response and parses it into a string array.
	 * @param response the server's response
	 * @return String[] the letters
	 */
	private String[] parseLetters(String response) {
		String[] responseParse = response.split(";");
		return responseParse;
	}
	/**
	 * verifyCenter is a method that verifies it the letter given matches the center letter.
	 * @param currentLetter the letter that's gonna get compared to the center letter
	 * @return boolean if it is the center letter or not
	 */
	private boolean verifyCenter(String currentLetter) {
		String centerLetter = gameClient.sendAndWaitMessage("getCenterLetter");
		if(currentLetter.equals(centerLetter)) {
			return true;
		}
		return false;
	}
}
