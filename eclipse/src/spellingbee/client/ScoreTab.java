package spellingbee.client;

import javafx.scene.control.Tab;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import spellingbee.network.Client;

/**
 * This score tab represents the goal of points that the player can reach
 * and their current score
 * @author Sirine Aoudj, 1935903
 *
 */
public class ScoreTab extends Tab{
	
	private Client gameClient;
	
	Text line1 = new Text("QueenBee: ");
	Text line2 = new Text("Genius: ");
	Text line3 = new Text("Amazing: ");
	Text line4 = new Text("Good: ");
	Text line5 = new Text("Getting Started: ");
	Text line6 = new Text("Current Score: ");
	
	private Text queenBee = new Text("");
	private Text genius = new Text("");
	private Text amazing = new Text("");
	private Text good = new Text("");
	private Text gettingStarted = new Text("");
	private Text currentScore = new Text("");

	/**
	 * This constructor initializes a score tab
	 * @param gameClient
	 */
	public ScoreTab(Client gameClient) {
		super("Score");
		this.gameClient = gameClient;
		this.setContent(addContent());
	}
	/**
	 * This method initialize all the lines of text for each achievement as well with the colors
	 * It also call getBrackets to change put the values of the current game on going
	 * then it organizes everything in a grid pane to have columns
	 * @return
	 */
	public GridPane addContent() {
		
		GridPane gridPane = new GridPane();
		
		this.line1.setFill(Color.GREY);
		this.line2.setFill(Color.GREY);
		this.line3.setFill(Color.GREY);
		this.line4.setFill(Color.GREY);
		this.line5.setFill(Color.GREY);
		this.line6.setFill(Color.BLUE);
		this.queenBee.setFill(Color.GREY);
		this.genius.setFill(Color.GREY);
		this.amazing.setFill(Color.GREY);
		this.good.setFill(Color.GREY);
		this.gettingStarted.setFill(Color.GREY);
		this.currentScore.setFill(Color.RED);
		
		
		gridPane.add(line1, 0, 0);
        gridPane.add(line2, 0, 1);
        gridPane.add(line3, 0, 2);
        gridPane.add(line4, 0, 3);
        gridPane.add(line5, 0, 4);
        gridPane.add(line6, 0, 5);
        gridPane.add(this.queenBee, 1, 0);
        gridPane.add(this.genius, 1, 1);
        gridPane.add(this.amazing, 1, 2);
        gridPane.add(this.good, 1, 3);
        gridPane.add(this.gettingStarted, 1, 4);
        gridPane.add(this.currentScore, 1, 5);
        
        gridPane.setHgap(10);
        setBrackets();
        return gridPane;
	}
	
	/**
	 * This method gets from the client the brackets then splits into a String[] where each value is the different 
	 * then it sets the tab text values to those from the string[]  
	 */
	public void setBrackets() {
	String response = gameClient.sendAndWaitMessage("getBrackets");
	String[] brackets = response.split(";");
	
	this.queenBee.setText(brackets[4]);
	this.genius.setText(brackets[3]);
	this.amazing.setText(brackets[2]);
	this.good.setText(brackets[1]);
	this.gettingStarted.setText(brackets[0]);
	}
	
	/**
	 * This method get the current score from the game client then change the gameClient currentScore text according to the score
	 */
	public void setCurrentScore() {
		String response2 = gameClient.sendAndWaitMessage("getScore");
		this.currentScore.setText(response2);
	}
	
	/**
	 * This method changes the color according to the amount of points collected
	 * Example: when the user reaches the getting started amount of points it will change the text to white as it was grey before 
	 */
	public void refresh() {
		
		setCurrentScore();
		int score = Integer.parseInt(this.currentScore.getText());
		int s25 = Integer.parseInt(this.gettingStarted.getText());
		int s50 = Integer.parseInt(this.good.getText());
		int s75 = Integer.parseInt(this.amazing.getText());
		int s90 = Integer.parseInt(this.genius.getText());
		int s100 = Integer.parseInt(this.queenBee.getText());
		
		if(score == s100) {
			this.queenBee.setFill(Color.WHITE);
			this.line1.setFill(Color.WHITE);
		}
		else if (score >= s90 && score < s100) {
			this.genius.setFill(Color.WHITE);
			this.line2.setFill(Color.WHITE);
		}
		else if (score >= s75 && score < s90) {
			this.amazing.setFill(Color.WHITE);
			this.line3.setFill(Color.WHITE);
		}
		else if (score >= s50 && score < s75) {
			this.good.setFill(Color.WHITE);
			this.line4.setFill(Color.WHITE);
		}
		else if (score >= s25 && score < s50) {
			this.genius.setFill(Color.WHITE);
			this.line5.setFill(Color.WHITE);
		}
		else if (score >= s25) {
			this.gettingStarted.setFill(Color.WHITE);
			this.line5.setFill(Color.WHITE);
		}
	}
	
}
