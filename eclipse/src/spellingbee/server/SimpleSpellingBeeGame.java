package spellingbee.server;
/**
 * SimpleSpellingBeeGame is a class that implements ISpellingBeeGame interface
 * SimpleSpellingBeeGame is the object representation of the backend of the game.  
 * It stores all the letters, the center letter, the total score and the brackets.  
 * The purpose of this simplified game is to make a somewhat playable spelling bee game
 * for the sake of testing the gui. 
 * @author David Nguyen
 *
 */
public class SimpleSpellingBeeGame implements ISpellingBeeGame {
	private String[] letters;
	private char centerLetter;
	private int totalScore;
	private int[] brackets;
	
	/**
	 * SimpleSpellingBeeGame constructor that initiates all the variables.
	 */
	public SimpleSpellingBeeGame() {
		final int INITIAL_SCORE = 0;
		final int GETTING_STARTED = 50;
		final int GOOD = 100;
		final int AMAZING = 150;
		final int GENIUS = 180;
		final int QUEEN_BEE = 200;
		this.letters = new String[]{"e","u","s","p","r","t","q"};
		this.centerLetter = 'e';
		this.totalScore = INITIAL_SCORE;
		this.brackets = new int[] {GETTING_STARTED,GOOD,AMAZING,GENIUS,QUEEN_BEE};
	}
	/**
	 * getBrackets returns the bracket of scores.
	 * @return int[] the bracket 
	 */
	public int[] getBrackets() {
		return this.brackets;
	}
	/**
	 * getAllLetters returns a string of all letters.
	 * @return String all letters
	 */
	public String getAllLetters() {
		String allLetters = "";
		for(int i = 0; i < letters.length; i++) {
			allLetters+=letters[i];
		}
		return allLetters;
	}
	/**
	 * getScore returns the current score
	 * @return int current score
	 */
	public int getScore() {
		return this.totalScore;
	}
	/**
	 * getCenterLetter returns the center letter
	 * @return char center letter
	 */
	public char getCenterLetter() {
		return this.centerLetter;
	}
	/**
	 * getMessage takes in a word attempt, verifies if the word contains the letter given
	 * and sends a message if it's a good word or a bad word.
	 * Note: The words being verified are not checked in the dictionary. 
	 * They're verified if the word contains the letter given and the center letter.
	 * @param attempt word attempt
	 * @return String message indicating good or bad
	 */
	public String getMessage(String attempt) {
		String message = "good!";
		String[] attemptSplit = attempt.split("");
		String centerLetter = Character.toString(getCenterLetter());
		
		message = verifyLetters(attemptSplit);
		//if the message is still good after verifying the letters, it will call verifyCenter to verify if it contains
		//the center letter
		if(message.equals("good!")) {
			message = verifyCenter(attemptSplit, centerLetter);
		}
		return message;
		
	}
	/**
	 * getPointsForWord takes in a word attempt and verify how much points it's worth.
	 * @param attempt word attempt
	 * @return int number of points
	 */
	public int getPointsForWord(String attempt) {
		final int POINT_1 = 4;
		final int POINT_LENGTH_WORD = 7;
		int score = 0;
		String message = getMessage(attempt);
		
		//verifies if the word is valid, once it's valid, the points will be assigned based off its length and the rules of spelling bee.
		if(message.equals("good!")) {
			if(attempt.length() <= POINT_1) {
				score = 1;
			}else if (attempt.length() > POINT_1 && attempt.length() < POINT_LENGTH_WORD) {
				score = attempt.length();
			}else if (attempt.length() >= POINT_LENGTH_WORD) {
				score = attempt.length() + POINT_LENGTH_WORD;
			}
		}
		//once the word is a valid word, the points will be added to the current score
		totalScore+=score;
		return score;
	}
	/*HELPER METHODS*/
	/**
	 * verifyCenter verifies if the word attempt contains the center letter
	 * @param attemptSplit the word attempt with each letter seperated in an array
	 * @param centerLetter the center letter
	 * @return String a message indicating if it contains the center letter
	 */
	private String verifyCenter(String[] attemptSplit, String centerLetter) {
		String message = "bad!";
		for(int i = 0; i < attemptSplit.length; i++) {
			if(attemptSplit[i].equals(centerLetter)) {
				message = "good!";
			}
		}
		return message;
	}
	/**
	 * verifyLetters verifies if the word attempt contains the letter given and if it does not
	 * @param attemptSplit the word attempt with each letter seperated in an array
	 * @return String a message indicating if it contains the letters given
	 */
	private String verifyLetters(String[] attemptSplit) {
		String message = "good!";
		boolean containLetter;
		for(int i = 0; i < attemptSplit.length; i++) {
			containLetter = false;
			for(int a = 0; a < letters.length; a++) {
				if(attemptSplit[i].equals(letters[a])) {
					containLetter = true;
				}
			}
			if(!containLetter) {
				message = "bad!";
				return message;
			}
		}
		return message;
	}
	
}
