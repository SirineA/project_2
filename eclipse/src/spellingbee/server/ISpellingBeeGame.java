package spellingbee.server;

/**
 * ISpellingBeeGame interface declares which method the spelling bee game needs to implement.
 * @author David Nguyen & Sirine Aoudj
 *
 */
public interface ISpellingBeeGame {
	int getPointsForWord(String attempt);
	String getMessage(String attempt);
	String getAllLetters();
	char getCenterLetter();
	int getScore();
	int[] getBrackets();
}
