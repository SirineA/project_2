package spellingbee.server;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * SpellingBeeGame implements the ISpellingBeeGame interface
 * store all the information necessary for a specific game
 * and the methods to get and or generate the information
 * @author Sirine Aoudj, 1935903
 *
 */
public class SpellingBeeGame implements ISpellingBeeGame{

	private String letters;
	private char centerLetter;
	private int score = 0;
	private List<String> guessedWords = new ArrayList<String>();
	private List<String> possibleWords = new ArrayList<String>();
	
	/**
	 * This constructor generates the random letters from the .txt file
	 * gets the center letter of those letters
	 * then initiates all the possible words with these letters and center letter
	 */
	public SpellingBeeGame() {
		this.letters = randomLetters();
		this.centerLetter = centerLetterGen(this.letters);
		this.possibleWords = possibleWords(this.letters, this.centerLetter);
	}
	
	/**
	 * This constructor takes as input a combination of letters
	 * then finds the center letter
	 * then makes a collection of the possible words
	 * @param letters combination of letters
	 */
	public SpellingBeeGame(String letters) {
		this.letters = letters;
		this.centerLetter = centerLetterGen(this.letters);
		this.possibleWords = possibleWords(this.letters, this.centerLetter);
	}
	
	/**
	 * This is to get a combination of letters of a SpellingBeeGame object
	 * @return String combination of letters of the object
	 */
	public String getAllLetters() {
		return this.letters;
	}
	
	/**
	 * This is to get a center letter of a SpellingBeeGame object
	 * @return char center letter of the object
	 */
	public char getCenterLetter() {
		return this.centerLetter;
	}
	
	/**
	 * This is to get the score of a SpellingBeeGame object
	 * @return int current score of the object
	 */
	public int getScore() {
		return this.score;
	}
	
	/**
	 * This is to get a arrayList of the words that the user guessed of a SpellingBeeGame object
	 * @return List<String> guessed words of the object
	 */
	public List<String> getGuesses() {
		return this.guessedWords;
	}
	
	/**
	 * This is to get a List<String> of the words that can be made with the combination of letters of the object of a SpellingBeeGame object
	 * @return List<String> possible words of the combination of letters of the object
	 */
	public List<String> getWords() {
		return this.possibleWords;
	}
	
	/**
	 * This method create a collection of words from a .txt file
	 * @param file is the string of the path of the file we want to convert into an List<String>
	 * @return List<String> that contains all the words of the dictionary given
	 */
	public List<String> createWordsFromFile(String file){
		
		List<String> word = new ArrayList<String>();
		Path path = Paths.get(file);
		
		try {
		    word = Files.readAllLines(path);
		}
		catch (IOException e) {
		    // Handle a potential exception
		}
		
		return word;
	}
	
	/**
	 * This method create a collection of combination of letters from a .txt file
	 * @param file is the string of the path of the file we want to convert into an List<String>
	 * @return List<String> that contains all the combinations of letters given
	 */
	public List<String> getPossibleLetters(String file){
		
		List<String> letterCombinations = new ArrayList<String>();;
		Path path = Paths.get(file);
		
		try {
			letterCombinations = Files.readAllLines(path);
		}
		catch (IOException e) {
		    // Handle a potential exception
		}
		
		
		return letterCombinations;
	}
	
	/**
	 * This method creates an list<string> of all the words of the dictionary
	 * then creates an alphabet that contains only the letters that cannot be used when making a possible word
	 * then it removes all the words from the dictionary that do not contain the center letter
	 * then it removes all the words from the dictionary that contain an unwanted letter
	 * then it removes all the words from the dictionary that are of invalid length (less than 4 letters)
	 * @param letters is the combination of letters used to find the possible words
	 * @param letterC is the center letter of letters
	 * @return List<String> words is all the possible words with the letters
	 */
	public List<String> possibleWords(String letters, char letterC){
		
		String basePath = new File("").getAbsolutePath();
		String filesFolder = basePath.replace("eclipse", "datafiles\\english.txt");
		List<String> words = createWordsFromFile(filesFolder);
		
		//Making every word of the dictionary to lower case to avoid equals errors
		for(int i = 0; i < words.size(); i++){
			words.set(i,words.get(i).toLowerCase());
		}
		String centerLetter = String.valueOf(letterC);  
		
	    List<String> lettersNotWanted = new ArrayList<String>(getUnwantedLetters(letters));
	    
	    words = getWordsWithCenterLetter(words,centerLetter);
	    words = getWordsWithValidLetters(words,lettersNotWanted);
	    words = verifyLength(words);
	
		return words;
		}
	
	/**
	 * This method calculates the max amount of points that an user can get with the combination of letters
	 * @return int[] of size 4 where there is the total amount of points in certain percentages, (25%,50%,75%,90%)
	 */
	public int[] getBrackets() {
		this.score =0;
		int totalPoints = 0;
		int points =0;
		int BONUSPOINTS = 7;
		
		for(int i = 0; i < this.possibleWords.size(); i++) {
			String[] letters = this.letters.split("");
			if(possibleWords.get(i).contains(letters[0]) && possibleWords.get(i).contains(letters[1]) && possibleWords.get(i).contains(letters[2]) 
				&& possibleWords.get(i).contains(letters[3]) && possibleWords.get(i).contains(letters[4]) && possibleWords.get(i).contains(letters[5]) 
				&& possibleWords.get(i).contains(letters[6])) {
					
					points = (possibleWords.get(i).length()) + (BONUSPOINTS);
				}
				else {
					points = possibleWords.get(i).length();
				}
			totalPoints += points;
			} 
		
		
		int[] percentages = new int[5];
		percentages[0] = (int)(totalPoints * 0.25);
		percentages[1] = (int)(totalPoints * 0.50);
		percentages[2] = (int)(totalPoints * 0.75);
		percentages[3] = (int)(totalPoints * 0.90);
		percentages[4] = totalPoints;
		
		return percentages;
		}
	
	/**
	 * 
	 */
	public int getPointsForWord(String word) {
		int points = 0;
		int BONUSPOINTS = 7;
		String[] letters = this.letters.split("");
		if(getMessage(word).equals("Good!")) {
			if(word.contains(letters[0]) && word.contains(letters[1]) && word.contains(letters[2]) && word.contains(letters[3]) &&
			   word.contains(letters[4]) && word.contains(letters[5]) && word.contains(letters[6])) {
				
				points = (word.length()) + (BONUSPOINTS);
			}
			else {
				points = word.length();
			}
		}
		this.guessedWords.add(word);
		this.score += points;
		return points;
	}
	
	/**
	 * This method verifies a word if its in the possible word if yes it will return "Good!", or else it will return "Invalid word... Try again"
	 * @param word is the word that is being verified
	 * @return message is good or not good depending if its a valid word or not
	 */
	public String getMessage(String word) {
		
		String message = "";
		
		for(int i = 0; i < this.possibleWords.size(); i++) {
			
			if(this.possibleWords.get(i).equals(word)) {
				
				for(int j = 0; j < this.guessedWords.size(); j++) {
					
					if(this.guessedWords.get(j).equals(word)) {
						message = "Already guessed!";
						return message;
					}
					
				}
				message = "Good!";
				return message;
			}
			else {
				message = "Invalid word... Try again";
			}
		}		
		this.guessedWords.add(word);
		return message;
	}
	
	//Helper Methods
	
	/**
	 * This method call the get possible letters then proceeds to take a random set of letters among this collection
	 * @return String letters that contain a set of letters
	 */
	public String randomLetters(){
	        
		String basePath = new File("").getAbsolutePath();
		String filesFolder = basePath.replace("eclipse", "datafiles\\letterCombinations.txt");
		List<String> letterCombinations = getPossibleLetters(filesFolder);
			
		Random rand = new Random();
			
		int index = rand.nextInt(letterCombinations.size());
	    String chosenLetters = letterCombinations.get(index);
	    return chosenLetters;
	    }
	
	/**
	 * This method find the center letter of the set of letters then converts it into a char
	 * @param letters is the set of letters of the object
	 * @return char center letter of the set of letters
	 */
	public char centerLetterGen(String letters) {
		char centerLetter;
		centerLetter = letters.charAt(3);
		return centerLetter;
		}
	
	/**
	 * This method takes as input a string letters that contains the combination of letters
	 * then it makes a string that contains all the letters of the alphabet
	 * then it will go through the alphabet and remove all the letter of the combination of letter 
	 * to get and List<String> of all the letters that are unwanted in the possible words
	 * @param letters string that contains the combination of letters
	 * @return List<String> of all the letters that are unwanted in the possible words
	 */
	public List<String> getUnwantedLetters(String letters){
		
		String alphabet = "abcdefghijklmnopqrstuvwxyz";
	    String[] letter = letters.split("");
	    char[] lettersChar = new char[letter.length];
	    
	    for(int i = 0; i < lettersChar.length; i++){
	    	lettersChar[i] = letter[i].charAt(0);
	    	}
	    
	    for(int j =0 ; j < lettersChar.length; j++) {
	    	alphabet = alphabet.replace(lettersChar[j], ' ');
			}

	    String[] temp = alphabet.split("");
	    List<String> lettersNotWanted= new ArrayList<String>();
	    
	    for(int l =0; l < temp.length; l++) {
			if(!temp[l].equals(" ")) {
				lettersNotWanted.add(temp[l]);
				}
			}
	    return lettersNotWanted;
	}
	
	/**
	 * This method removes all the words from the dictionary that do not have the center letter as a possible word requires the center letter
	 * @param words is the dictionary
	 * @param centerLetter is the center letter
	 * @return words the dictionary without the words that do not have the center letter
	 */
	public List<String> getWordsWithCenterLetter(List<String> words, String centerLetter){
		
		for(int i = 0; i < words.size(); i++) {
			if(!words.get(i).toLowerCase().contains(centerLetter)) {
				words.remove(i);
				if(i != 0) {
					i-=1;
				}
			}
		}
		return words;
		
	}
	
	/**
	 * This method removes all the words from the dictionary that do  have an unwanted letter 
	 * as a possible word can only have letters from the set of letters
	 * @param words is the dictionary
	 * @param lettersNotWanted contains all the letters that cannot be present in a word
	 * @return words the dictionary without the words that contain an unwanted letter
	 */
	public List<String> getWordsWithValidLetters(List<String> words, List<String> lettersNotWanted){
		
		for(int i = 0; i < words.size(); i++) {
			for(int u = 0; u < lettersNotWanted.size(); u++) {
				//System.out.println("h = " + h + "u = " + u);
				if(words.get(i).toLowerCase().contains(lettersNotWanted.get(u))) {
					words.remove(i);
					/*System.out.println("h= " + h);
					System.out.println(words.size());*/
					if(i != 0) {
						i-=1;
					}	
				}
			}
		}
		return words;
	}
	
	/**
	 * This method removes all the words from the dictionary that have a length of < 4
	 * @param words is the dictionary
	 * @return words the dictionary without the words that have a length of < 4
	 */
	public List<String> verifyLength(List<String> words){
		
		for(int h = 0; h < words.size(); h++) {
			if(words.get(h).length() < 4) {
				words.remove(h);
				if(h != 0 ) {
					h-=1;
				}
			}
		}
		return words;
	}
	
}
		
	
