package spellingbee.network;

import spellingbee.server.*;

/**
 * This class will run on the server side and is used to connect the server code to the backend business code.
 * This class is where the "protocol" will be defined.
 */
public class ServerController {
	// This is the interface you will be creating!
	// Since we are storing the object as an interface, you can use any type of ISpellingBeeGame object.
	// This means you can work with either the SpellingBeeGame OR SimpleSpellingBeeGame objects and can
	// seamlessly change between the two.
	private ISpellingBeeGame spellingBee = new SpellingBeeGame();
	
	/**
	 * Action is the method where the protocol translation takes place.
	 * This method is called every single time that the client sends a request to the server.
	 * It takes as input a String which is the request coming from the client. 
	 * It then does some actions on the server (using the ISpellingBeeGame object)
	 * and returns a String representing the message to send to the client
	 * @param inputLine The String from the client
	 * @return The String to return to the client
	 */
	public String action(String inputLine) {
		final int INDEX_START = 0;
		final int INDEX_WORD = 1;
		final int REMOVE_LAST_CHAR = 1;
		final int CONTAINS_ATTEMPT = 2;
		String[] inputLineSplit = inputLine.split(";");
		String attempt = "";
		String response = "";
		//if there is a word attempt being sent, the word attempt will be assigned to a variable
		if(inputLineSplit.length == CONTAINS_ATTEMPT) {
			attempt = inputLineSplit[INDEX_WORD];
		}
		
		//gets the point for the word attempt
		if(inputLine.equals("getPointsForWord;"+attempt)) {
			response = Integer.toString(spellingBee.getPointsForWord(attempt));
			return response;
		
		//gets the message if the word attempt is valid or not
		}else if(inputLine.equals("getMessage;"+attempt)) {
			response = spellingBee.getMessage(attempt);
			return response;
		
		//gets all the letters given
		}else if(inputLine.equals("getAllLetters")) {
			String allLetters = spellingBee.getAllLetters();
			String[] splitAllLetters = allLetters.split("");
			for(int i = 0; i < splitAllLetters.length; i++) {
				response+=splitAllLetters[i]+";";
			}
			response = response.substring(INDEX_START,response.length()-REMOVE_LAST_CHAR);
			return response;
		
		//gets the center letter
		}else if(inputLine.equals("getCenterLetter")) {
			response = Character.toString(spellingBee.getCenterLetter());
			return response;
		
		//gets the current user score
		}else if(inputLine.equals("getScore")) {
			response = Integer.toString(spellingBee.getScore());
			return response;
		
		//gets the brackets of score
		}else if(inputLine.equals("getBrackets")) {
			int[] brackets = spellingBee.getBrackets();
			for(int i = 0; i < brackets.length; i++) {
				response+=brackets[i]+";";
			}
			response = response.substring(INDEX_START,response.length()-REMOVE_LAST_CHAR);
			return response;
		}
		return null;
		
		/* Your code goes here!!!!
		 * Note: If you want to preserve information between method calls, then you MUST
		 * store it into private fields.
		 * You should use the spellingBee object to make various calls and here is where your communication
		 * code/protocol should go.
		 * For example, based on the samples in the assignment:
		 * if (inputLine.equals("getCenter")) {
		 * 	// client has requested getCenter. Call the getCenter method which returns a String of 7 letters
		 *      return spellingBee.getCenter();
		 * }
		 * else if ( ..... )
		return null;*/
		
	}
}
