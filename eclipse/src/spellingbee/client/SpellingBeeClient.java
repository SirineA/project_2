package spellingbee.client;

import spellingbee.network.*;

import javafx.beans.value.ChangeListener;

import javafx.application.*;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
/**
 * SpellingBeeClient is a class that sets up the spelling bee GUI and displays it.
 * @author David Nguyen & Sirine Aoudj
 *
 */
public class SpellingBeeClient extends Application{
	private Client gameClient = new Client();
	/**
	 * start sets up the scene
	 * @param Stage stage
	 */
	public void start(Stage stage) {
		final int WIDTH = 650;
		final int HEIGHT = 300;
		Group root = new Group();
		
		Scene scene = new Scene(root,WIDTH,HEIGHT);
		scene.setFill(Color.BLACK);
		
		TabPane tb = new TabPane();
		GameTab tab1 = new GameTab(gameClient);
		ScoreTab tab2 = new ScoreTab(gameClient);
		
		tb.getTabs().add(tab1);
		tb.getTabs().add(tab2);
		
		root.getChildren().add(tb);
		
		stage.setScene(scene);
		stage.show();
		 /*This get the tab1 score value and when it gets changed it calls the refresh() method on the score tab to change the properties according to the score*/
		tab1.getScoreField().textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable,
					String oldValue, String newValue) {
				
				tab2.refresh();
			}
		});
	}
	/**
	 * main launches the application
	 * @param args
	 */
	public static void main(String[]args) {
		Application.launch(args);
	}
}
